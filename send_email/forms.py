from django import forms

from .models import SendMail

class SendMailForm(forms.ModelForm):

    class Meta:
        model = SendMail
        fields = [
            'user_name','user_mail', 'user_message'
        ]
