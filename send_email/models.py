from django.db import models

# Create your models here.
class SendMail(models.Model):
    user_name = models.CharField(verbose_name = 'Name', max_length=256)
    user_mail = models.EmailField(verbose_name = 'Email')
    user_message = models.CharField(verbose_name = 'Message', max_length=256)
