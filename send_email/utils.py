from django.core.mail import send_mail
from django.conf import settings


def send_email_request(user_name, user_mail, user_message):
    mail_title = 'Question from user'
    mail_body = 'Say hello\n'
    mail_body += 'Name: %s\n' % user_name
    mail_body += 'Email: %s\n' % user_mail
    mail_body += 'Message: %s\n' % user_message
    send_mail(mail_title, mail_body, 'localhost@server.ru', ['dizik93@gmail.com'])
