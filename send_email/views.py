from django.shortcuts import render
from django.views.generic import FormView
from django.http import JsonResponse
from .forms import SendMailForm
from .utils import send_email_request

# Create your views here.
# def index(request):
#     return render(request, 'index.html')


class SendMailView(FormView):
    http_method_names = ['post']
    form_class = SendMailForm

    def form_valid(self, form):
        form.save()
        send_email_request(
            form.cleaned_data['user_name'],
            form.cleaned_data['user_mail'],
            form.cleaned_data['user_message'],
        )
        return JsonResponse({
            'status': 'success'
        })

    def form_invalid(self, form):
        return JsonResponse({
            'status': 'error',
            'errors': form.errors
        })
