window.addEventListener('DOMContentLoaded', function () {

    let tab = document.querySelectorAll('.info__header-tab'),
        info = document.querySelector('.info__header'),
        tabContent = document.querySelectorAll('.info__tabcontent');

    function hideTabContent(a) {
        for (let i = a; i < tabContent.length; i++) {
            tabContent[i].classList.remove('show');
            tabContent[i].classList.add('hide');
        }
    }

    hideTabContent(1);

    function showTabContent(b) {
        if (tabContent[b].classList.contains('hide')) {
            tabContent[b].classList.remove('hide');
            tabContent[b].classList.add('show');
        }
    }

    info.addEventListener('click', function (event) {
        let target = event.target;
        if (target && target.classList.contains('info__header-tab')) {
            for (let i = 0; i < tab.length; i++) {
                if (target == tab[i]) {
                    hideTabContent(0);
                    showTabContent(i);
                    break;
                }
            }
        }

    });

    function calcCount() {
        for (var i = 0; i < $('.items__lists-number').length; i++) {
            var end = $('.items__lists-number').eq(i).text();
            countStart(end, i);
        }
    }

    function countStart(end, i) {
        var start = 0;
        var interval = setInterval(function () {
            $('.items__lists-number').eq(i).text(++start);
            if (start == end) {
                clearInterval(interval);
            }
        }, 1);
    }

    $(function () {
        var jqBar = $('.discuss');
        var jqBarStatus = true;
        $(window).scroll(function () {
          var scrollEvent = ($(window).scrollTop() > (jqBar.position().top - $(window).height()));
          if (scrollEvent && jqBarStatus) {
            jqBarStatus = false;
            calcCount();
          }
        });
      });
    

});